<?php


namespace ComposePress\Cli;


use ComposePress\Versioner\Config;
use ComposePress\Versioner\DefaultCodeVersioner;
use ComposePress\Versioner\Exception\Config as ConfigException;
use ComposePress\Versioner\FileNameVersioner;
use ComposePress\Versioner\FileNameVersionerProgress;
use Composer\Semver\VersionParser;
use UnexpectedValueException;
use WP_CLI;

class Versioner extends \WP_CLI_Command {
	/**
	 * Change version of a composepress package (not a composepress plugin)
	 *
	 * ## OPTIONS
	 *
	 * [--version=<string>]
	 * : Specify specific version of ComposePress to use
	 *
	 * ## EXAMPLES
	 *
	 *     # Basic use.
	 *     wp composepress versioner update --version=2.0.0
	 *
	 * @throws \WP_CLI\ExitException
	 */
	public function update( $args, $assoc_args ) {
		$version = \WP_CLI\Utils\get_flag_value( $assoc_args, 'version' );
		if ( empty( $version ) ) {
			WP_CLI::error( 'Version is required' );
		}

		try {
			$version_parser = new VersionParser();
			$version        = $version_parser->normalize( $version );
		} catch ( UnexpectedValueException $value_exception ) {
			WP_CLI::error( $value_exception->getMessage() );
		}

		try {
			$filename_versioner = new FileNameVersioner( new DefaultCodeVersioner(), Config::find(), $version, new class implements FileNameVersionerProgress {
				private $total;
				/*
				 * @var \cli\progress\Bar
				 */
				private $bar;

				public function __invoke( string $name, array $data ) {
					if ( \cli\Shell::isPiped() ) {
						return;
					}
					if ( FileNameVersioner::PROGRESS_START === $name ) {
						$this->total = (int) $data['total'];
						$this->bar   = new Bar( 'Processing files', $this->total );
					}
					if ( FileNameVersioner::PROGRESS_UPDATE === $name ) {
						$this->bar->set_message( $data['file'] );
						$this->bar->tick();

						if ( $this->bar->get_current() > $this->total ) {
							$this->bar->finish();
						}
					}
				}
			} );
		} catch ( ConfigException $exception ) {
			WP_CLI::error( $exception->getMessage() );
		}

		$filename_versioner->run();
	}

}
