<?php

namespace ComposePress\Cli\Utils;

/*
		 * Copied from wp-includes/formatting.php:sanitize_title_with_dashes
		 */
function create_slug( $data ) {
	$data = strip_tags( $data );
	// Preserve escaped octets.
	$data = preg_replace( '|%([a-fA-F0-9][a-fA-F0-9])|', '---$1---', $data );
	// Remove percent signs that are not part of an octet.
	$data = str_replace( '%', '', $data );
	// Restore octets.
	$data = preg_replace( '|---([a-fA-F0-9][a-fA-F0-9])---|', '%$1', $data );

	if ( seems_utf8( $data ) ) {
		if ( function_exists( 'mb_strtolower' ) ) {
			$data = mb_strtolower( $data, 'UTF-8' );
		}
		$data = utf8_uri_encode( $data, 200 );
	}

	$data = strtolower( $data );

	$data = preg_replace( '/&.+?;/', '', $data ); // kill entities
	$data = str_replace( '.', '-', $data );

	$data = preg_replace( '/[^%a-z0-9 _-]/', '', $data );
	$data = preg_replace( '/\s+/', '-', $data );
	$data = preg_replace( '|-+|', '-', $data );
	$data = trim( $data, '-' );

	return $data;
}

/*
 * Copied from wp-includes/formatting.php
 */
function seems_utf8( $str ) {
	mbstring_binary_safe_encoding();
	$length = strlen( $str );
	mbstring_binary_safe_encoding( true );
	for ( $i = 0; $i < $length; $i ++ ) {
		$c = ord( $str[ $i ] );
		if ( $c < 0x80 ) {
			$n = 0;
		} // 0bbbbbbb
		elseif ( ( $c & 0xE0 ) == 0xC0 ) {
			$n = 1;
		} // 110bbbbb
		elseif ( ( $c & 0xF0 ) == 0xE0 ) {
			$n = 2;
		} // 1110bbbb
		elseif ( ( $c & 0xF8 ) == 0xF0 ) {
			$n = 3;
		} // 11110bbb
		elseif ( ( $c & 0xFC ) == 0xF8 ) {
			$n = 4;
		} // 111110bb
		elseif ( ( $c & 0xFE ) == 0xFC ) {
			$n = 5;
		} // 1111110b
		else {
			return false;
		} // Does not match any model
		for ( $j = 0; $j < $n; $j ++ ) { // n bytes matching 10bbbbbb follow ?
			if ( ( ++ $i == $length ) || ( ( ord( $str[ $i ] ) & 0xC0 ) != 0x80 ) ) {
				return false;
			}
		}
	}

	return true;
}

function mbstring_binary_safe_encoding( $reset = false ) {
	static $encodings = array();
	static $overloaded = null;

	if ( is_null( $overloaded ) ) {
		$overloaded = function_exists( 'mb_internal_encoding' ) && ( ini_get( 'mbstring.func_overload' ) & 2 );
	}

	if ( false === $overloaded ) {
		return;
	}

	if ( ! $reset ) {
		$encoding = mb_internal_encoding();
		array_push( $encodings, $encoding );
		mb_internal_encoding( 'ISO-8859-1' );
	}

	if ( $reset && $encodings ) {
		$encoding = array_pop( $encodings );
		mb_internal_encoding( $encoding );
	}
}

function utf8_uri_encode( $utf8_string, $length = 0 ) {
	$unicode        = '';
	$values         = array();
	$num_octets     = 1;
	$unicode_length = 0;

	mbstring_binary_safe_encoding();
	$string_length = strlen( $utf8_string );
	mbstring_binary_safe_encoding( true );

	for ( $i = 0; $i < $string_length; $i ++ ) {

		$value = ord( $utf8_string[ $i ] );

		if ( $value < 128 ) {
			if ( $length && ( $unicode_length >= $length ) ) {
				break;
			}
			$unicode .= chr( $value );
			$unicode_length ++;
		} else {
			if ( count( $values ) == 0 ) {
				if ( $value < 224 ) {
					$num_octets = 2;
				} elseif ( $value < 240 ) {
					$num_octets = 3;
				} else {
					$num_octets = 4;
				}
			}

			$values[] = $value;

			if ( $length && ( $unicode_length + ( $num_octets * 3 ) ) > $length ) {
				break;
			}
			if ( count( $values ) == $num_octets ) {
				for ( $j = 0; $j < $num_octets; $j ++ ) {
					$unicode .= '%' . dechex( $values[ $j ] );
				}

				$unicode_length += $num_octets * 3;

				$values     = array();
				$num_octets = 1;
			}
		}
	}

	return $unicode;
}
