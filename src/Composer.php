<?php


namespace ComposePress\Cli;


use Composer\Console\Application;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;

class Composer {
	/**
	 * @param $arguments
	 */
	public static function run_command( $arguments ) {
		self::increase_memory();

		putenv( 'COMPOSER_NO_INTERACTION=1' );
		$home = self::get_user_home();
		if ( ! empty( $home ) ) {
			putenv( "COMPOSER_HOME={$home}/.composer" );
		}

		array_unshift( $arguments, null );

		$app = new Application();
		$app->setAutoExit( false );
		$output = new BufferedOutput();
		$input  = new ArgvInput( $arguments );

		$result = $app->run( $input, $output );

		return [ $result, $output ];
	}

	public static function increase_memory() {
		$memory_limit = trim( ini_get( 'memory_limit' ) );
		// Increase memory_limit if it is lower than 1.5GB
		if ( - 1 !== $memory_limit && self::memory_in_bytes( $memory_limit ) < 1024 * 1024 * 1536 ) {
			@ini_set( 'memory_limit', - 1 );
		}
	}

	private static function get_user_home() {
		// Cannot use $_SERVER superglobal since that's empty during UnitUnishTestCase
		// getenv('HOME') isn't set on Windows and generates a Notice.
		$home = getenv( 'HOME' );
		if ( ! empty( $home ) ) {
			// home should never end with a trailing slash.
			$home = rtrim( $home, '/' );
		} elseif ( ! empty( $_SERVER['HOMEDRIVE'] ) && ! empty( $_SERVER['HOMEPATH'] ) ) {
			// home on windows
			$home = $_SERVER['HOMEDRIVE'] . $_SERVER['HOMEPATH'];
			// If HOMEPATH is a root directory the path can end with a slash. Make sure
			//// that doesn't happen.
			$home = rtrim( $home, '\\/' );
		}

		return empty( $home ) ? null : $home;
	}

	private static function memory_in_bytes( $value ) {
		$unit  = strtolower( substr( $value, - 1, 1 ) );
		$value = (int) $value;
		switch ( $unit ) {
			case 'g':
				$value *= 1024;
			// no break (cumulative multiplier) @noinspection PhpMissingBreakStatementInspection
			case 'm':
				$value *= 1024;
			// no break (cumulative multiplier) @noinspection PhpMissingBreakStatementInspection
			case 'k':
				$value *= 1024;
			// no break (cumulative multiplier) @noinspection PhpMissingBreakStatementInspection
		}

		return $value;
	}
}
