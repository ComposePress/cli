<?php

namespace ComposePress\Cli;

use Composer\Factory;
use Composer\IO\BufferIO;
use Composer\Json\JsonFile;
use Composer\Package\CompletePackage;
use Composer\Repository\RepositoryFactory;
use Composer\Semver\Constraint\Constraint;
use Composer\Semver\Semver;
use WP_CLI;
use function ComposePress\Cli\Utils\create_slug;
use function WP_CLI\Utils\mustache_render;

/**
 * Class ComposePress
 *
 * @package ComposePress\Cli
 * @when    before_wp_load
 */
class Main extends \WP_CLI_Command {
	/**
	 * Create a new project
	 *
	 * ## OPTIONS
	 *
	 * [<plugin_name>...]
	 * : The name of the plugin
	 *
	 * [--force]
	 * : Force overwrite files
	 *
	 * [--version=<string>]
	 * : Specify specific version of ComposePress to use
	 *
	 * [--namespaced-functions]
	 * : Enable plugin functions to be added to plugin namespace and not prefixed with plugin slug
	 *
	 * [--dir=<string>]
	 * : Install to a different directory
	 *
	 *
	 * ## EXAMPLES
	 *
	 *     # Basic use.
	 *     wp composepress create-project My Plugin
	 *
	 *     # Force overwrite files
	 *     wp composepress create-project My Plugin --force
	 *
	 *     # Use Specific ComposePress version
	 *     wp composepress create-project My Plugin --version=1.2.3
	 *
	 *     # Enable function namespacing
	 *     wp composepress create-project My Plugin --namespaced-functions
	 *
	 *     # Create project in different directory
	 *     wp composepress create-project My Plugin --dir=./new-dir
	 *
	 * @subcommand create-project
	 * @throws \WP_CLI\ExitException
	 */
	public function create_project( $args, $assoc_args ) {
		$plugin_name = implode( ' ', $args );

		if ( empty( $plugin_name ) ) {
			WP_CLI::error( 'Plugin name is required' );
		}

		$dir = \WP_CLI\Utils\get_flag_value( $assoc_args, 'dir' );

		$plugin_slug      = create_slug( $plugin_name );
		$plugin_safe_slug = str_replace( '-', '_', $plugin_slug );

		if ( empty( $dir ) ) {
			$dir = $plugin_slug;
		}

		if ( ! is_dir( $dir ) && ! mkdir( $dir ) && ! is_dir( $dir ) ) {
			WP_CLI::error( sprintf( 'Could not create directory %s. Please check permissions and try again', $dir ) );
		}

		if ( ! empty( $dir ) ) {
			chdir( $dir );
		}

		/** @var \cli\progress\Bar $progress */
		$progress = \WP_CLI\Utils\make_progress_bar( 'Creating project', 5 );


		$name_parts = explode( '_', $plugin_safe_slug );
		$name_parts = array_map( 'ucfirst', $name_parts );
		$namespace  = implode( '_', $name_parts );

		if ( isset( $assoc_args['namespace'] ) ) {
			$namespace       = $assoc_args['namespace'];
			$namespace       = create_slug( $namespace );
			$namespace       = str_replace( '-', '_', $namespace );
			$namespace_parts = explode( '_', $namespace );
			$namespace_parts = array_map( 'ucfirst', $namespace_parts );
			$namespace       = implode( '_', $namespace_parts );
		}

		$latest_version = \WP_CLI\Utils\get_flag_value( $assoc_args, 'version' );
		if ( empty( $latest_version ) ) {
			$latest_version = $this->get_latest_component_version( 'core' );
		}
		$latest_version_slug = str_replace( '.', '_', $latest_version );

		Composer::run_command( [
			'init',
			'--name=' . get_current_user() . '/' . $plugin_safe_slug,
			'--type=' . 'wordpress-plugin',
			'--require=' . "composepress/core:$latest_version",
		] );

		$progress->tick();


		Composer::run_command( [
			'install',
		] );

		$progress->tick();

		$vars = [
			'plugin_name'      => $plugin_name,
			'plugin_namespace' => $namespace,
			'plugin_slug'      => $plugin_slug,
			'plugin_safe_slug' => $plugin_safe_slug,
		];

		$templates  = [
			"{$plugin_slug}.php" => 'plugin-entry.mustache',
			'config_prod.php'    => 'plugin-container-config.mustache',
			'src/Plugin.php'     => 'plugin-class.mustache',
		];
		$namespaced = \WP_CLI\Utils\get_flag_value( $assoc_args, 'namespaced-functions' );
		if ( $namespaced ) {
			$templates["{$plugin_slug}.php"] = 'plugin-entry-namespaced.mustache';
		}

		$components = [];
		foreach ( [ 'Plugin', 'Component', 'Manager' ] as $component ) {
			$components[] = [ 'name' => $component, 'version' => "\\v{$latest_version_slug}" ];
		}
		$templates["src/framework.php"] = [
			'framework.mustache',
			[ 'components' => $components ],
		];

		foreach ( $templates as $file => $template ) {
			$template_vars = $vars;
			$template_name = $template;
			if ( is_array( $template ) ) {
				/** @noinspection SlowArrayOperationsInLoopInspection */
				$template_vars = array_merge( $template_vars, $template[1] );
				$template_name = $template[0];
			}
			$templates[ $file ] = mustache_render( $this->get_template_path( $template_name ), $template_vars );
		}
		$force         = \WP_CLI\Utils\get_flag_value( $assoc_args, 'force' );
		$files_written = $this->create_files( $templates, $force );

		$progress->tick();

		$json = new JsonFile( './composer.json' );

		$json_data             = $json->read();
		$json_data['autoload'] = [
			'files' => [ 'src/framework.php' ],
			'psr-4' => [
				"$namespace\\" => 'src\\',
			],
		];

		$json->write( $json_data );

		$progress->tick();

		Composer::run_command( [
			'dumpautoload',
		] );

		$progress->tick();

		$progress->finish();

		$this->log_whether_files_written(
			$files_written,
			$skip_message = 'All plugin files were skipped.',
			$success_message = 'Created plugin project.'
		);
	}

	/**
	 * @param string $component
	 *
	 * @return bool|string
	 */
	private function get_latest_component_version( $component ) {
		Composer::increase_memory();

		$io     = new BufferIO();
		$config = Factory::createConfig( $io );
		$rm     = RepositoryFactory::manager( $io, $config, null, Factory::createRemoteFilesystem( $io, $config ) );
		$repos  = RepositoryFactory::defaultRepos( $io, $config, $rm );
		foreach ( $repos as $repo ) {
			$rm->addRepository( $repo );
		}

		$packages = $this->get_component_package( $component, new Constraint( '>', 0 ) );
		$versions = array_filter( $packages, function ( CompletePackage $package ) {
			return false === strpos( $package->getVersion(), '-' );
		} );
		$versions = array_map( function ( CompletePackage $package ) {
			return $package->getVersion();
		}, $versions );

		if ( empty( $versions ) ) {
			return false;
		}

		$versions = Semver::rsort( $versions );

		return $versions[0];
	}

	private function get_component_package( $component, Constraint $constraint ) {
		$io     = new BufferIO();
		$config = Factory::createConfig( $io );
		$rm     = RepositoryFactory::manager( $io, $config, null, Factory::createRemoteFilesystem( $io, $config ) );
		$repos  = RepositoryFactory::defaultRepos( $io, $config, $rm );
		foreach ( $repos as $repo ) {
			$rm->addRepository( $repo );
		}

		return $rm->findPackages( false !== strpos( $component, '/' ) ? $component : "composepress/{$component}", $constraint );
	}

	/**
	 * Gets the template path based on installation type.
	 *
	 * @throws \WP_CLI\ExitException
	 */
	private function get_template_path( $template ) {
		$command_root  = WP_CLI\Utils\phar_safe_path( dirname( __DIR__ ) );
		$template_path = "{$command_root}/templates/{$template}";

		if ( ! file_exists( $template_path ) ) {
			WP_CLI::error( "Couldn't find {$template}" );
		}

		return $template_path;
	}

	private function create_files( $files_and_contents, $force ) {
		$wrote_files = array();

		foreach ( $files_and_contents as $filename => $contents ) {
			$should_write_file = $this->prompt_if_files_will_be_overwritten( $filename, $force );
			if ( ! $should_write_file ) {
				continue;
			}
			$dir = dirname( $filename );
			if ( ! is_dir( $dir ) && ! mkdir( $dir ) && ! is_dir( $dir ) ) {
				WP_CLI::error( sprintf( 'Could not create directory %s. Please check permissions and try again', $dir ) );
			}

			if ( ! file_put_contents( $filename, $contents ) ) {
				WP_CLI::error( "Error creating file: $filename" );
			} elseif ( $should_write_file ) {
				$wrote_files[] = $filename;
			}
		}

		return $wrote_files;
	}

	private function prompt_if_files_will_be_overwritten( $filename, $force ) {
		$should_write_file = true;
		if ( ! file_exists( $filename ) ) {
			return true;
		}

		WP_CLI::warning( 'File already exists.' );
		WP_CLI::log( $filename );
		if ( ! $force ) {
			do {
				$answer = \cli\prompt(
					'Skip this file, or replace it with scaffolding?',
					$default = false,
					$marker = '[s/r]: '
				);
			} while ( ! in_array( $answer, array( 's', 'r' ) ) );
			$should_write_file = 'r' === $answer;
		}

		$outcome = $should_write_file ? 'Replacing' : 'Skipping';
		WP_CLI::log( $outcome . PHP_EOL );

		return $should_write_file;
	}

	private function log_whether_files_written( $files_written, $skip_message, $success_message ) {
		if ( empty( $files_written ) ) {
			WP_CLI::log( $skip_message );
		} else {
			WP_CLI::success( $success_message );
		}
	}

	/**
	 * Create a new project
	 *
	 * ## OPTIONS
	 *
	 * [<component>]
	 * : The name of the plugin
	 *
	 * [--force]
	 * : Force overwrite files
	 *
	 * [--version=<string>]
	 * : Specify specific version of ComposePress to use
	 *
	 *
	 * ## EXAMPLES
	 *
	 *     # Basic use.
	 *     wp composepress create-project My Plugin
	 *
	 *     # Force overwrite files
	 *     wp composepress create-project My Plugin --force
	 *
	 *     # Use Specific ComposePress version
	 *     wp composepress create-project My Plugin --version=1.2.3
	 *
	 *     # Enable function namespacing
	 *     wp composepress create-project My Plugin --namespaced-functions
	 *
	 *     # Create project in different directory
	 *     wp composepress create-project My Plugin --dir=./new-dir
	 *
	 * @subcommand add-component
	 * @throws \WP_CLI\ExitException
	 */
	public function add_component( $args, $assoc_args ) {

		$component = array_shift( $args );

		WP_CLI::log( "Searching for component {$component}" );

		$latest_version = \WP_CLI\Utils\get_flag_value( $assoc_args, 'version' );
		if ( empty( $latest_version ) ) {
			$latest_version = $this->get_latest_component_version( $component );
		}

		if ( empty( $latest_version ) ) {
			WP_CLI::error( "Component {$component} was not found" );
		}

		$latest_version_slug = str_replace( '.', '_', $latest_version );

		Composer::run_command( [
			'require',
			false !== strpos( $component, '/' ) ? $component : "composepress/{$component}",
			$latest_version,
		] );
	}
}
