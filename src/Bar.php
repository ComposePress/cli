<?php


namespace ComposePress\Cli;


class Bar extends \cli\progress\Bar {
	/**
	 * @return int
	 */
	public function get_current(): int {
		return $this->_current;
	}

	/**
	 * @param string $message
	 */
	public function set_message( string $message ) {
		$this->_message = $message;
	}
}
